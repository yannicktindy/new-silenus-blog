<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230802074329 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article_dev_logo (article_id INT NOT NULL, dev_logo_id INT NOT NULL, INDEX IDX_1F1715C87294869C (article_id), INDEX IDX_1F1715C8D7180B63 (dev_logo_id), PRIMARY KEY(article_id, dev_logo_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_com (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, comment LONGTEXT NOT NULL, mark INT DEFAULT NULL, INDEX IDX_F6A9E59C7294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article_dev_logo ADD CONSTRAINT FK_1F1715C87294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_dev_logo ADD CONSTRAINT FK_1F1715C8D7180B63 FOREIGN KEY (dev_logo_id) REFERENCES dev_logo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_com ADD CONSTRAINT FK_F6A9E59C7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66F98F144A');
        $this->addSql('DROP INDEX IDX_23A0E66F98F144A ON article');
        $this->addSql('ALTER TABLE article DROP logo_id, CHANGE title title VARCHAR(255) NOT NULL, CHANGE slug slug VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE dev_logo ADD is_dev TINYINT(1) NOT NULL, DROP link');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_dev_logo DROP FOREIGN KEY FK_1F1715C87294869C');
        $this->addSql('ALTER TABLE article_dev_logo DROP FOREIGN KEY FK_1F1715C8D7180B63');
        $this->addSql('ALTER TABLE article_com DROP FOREIGN KEY FK_F6A9E59C7294869C');
        $this->addSql('DROP TABLE article_dev_logo');
        $this->addSql('DROP TABLE article_com');
        $this->addSql('ALTER TABLE article ADD logo_id INT DEFAULT NULL, CHANGE title title VARCHAR(50) NOT NULL, CHANGE slug slug VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66F98F144A FOREIGN KEY (logo_id) REFERENCES dev_logo (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_23A0E66F98F144A ON article (logo_id)');
        $this->addSql('ALTER TABLE dev_logo ADD link VARCHAR(255) NOT NULL, DROP is_dev');
    }
}
