<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230731175527 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article_article_categ (article_id INT NOT NULL, article_categ_id INT NOT NULL, INDEX IDX_F3E25CC77294869C (article_id), INDEX IDX_F3E25CC7BDFFAF79 (article_categ_id), PRIMARY KEY(article_id, article_categ_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article_article_categ ADD CONSTRAINT FK_F3E25CC77294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_article_categ ADD CONSTRAINT FK_F3E25CC7BDFFAF79 FOREIGN KEY (article_categ_id) REFERENCES article_categ (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article ADD category1 VARCHAR(50) NOT NULL, ADD category2 VARCHAR(50) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_article_categ DROP FOREIGN KEY FK_F3E25CC77294869C');
        $this->addSql('ALTER TABLE article_article_categ DROP FOREIGN KEY FK_F3E25CC7BDFFAF79');
        $this->addSql('DROP TABLE article_article_categ');
        $this->addSql('ALTER TABLE article DROP category1, DROP category2');
    }
}
