<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230606133203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE hat (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, img VARCHAR(255) NOT NULL, matter VARCHAR(20) NOT NULL, shape VARCHAR(20) NOT NULL, is_promo TINYINT(1) NOT NULL, quantity INT NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE revcard (id INT AUTO_INCREMENT NOT NULL, reverso_id INT DEFAULT NULL, img VARCHAR(255) NOT NULL, name VARCHAR(10) NOT NULL, INDEX IDX_FF60D235B6AECFCF (reverso_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reverso (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(40) NOT NULL, img VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE revscore (id INT AUTO_INCREMENT NOT NULL, reverso_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL, count INT DEFAULT NULL, points INT DEFAULT NULL, INDEX IDX_7B190456B6AECFCF (reverso_id), INDEX IDX_7B190456A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE revcard ADD CONSTRAINT FK_FF60D235B6AECFCF FOREIGN KEY (reverso_id) REFERENCES reverso (id)');
        $this->addSql('ALTER TABLE revscore ADD CONSTRAINT FK_7B190456B6AECFCF FOREIGN KEY (reverso_id) REFERENCES reverso (id)');
        $this->addSql('ALTER TABLE revscore ADD CONSTRAINT FK_7B190456A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE revcard DROP FOREIGN KEY FK_FF60D235B6AECFCF');
        $this->addSql('ALTER TABLE revscore DROP FOREIGN KEY FK_7B190456B6AECFCF');
        $this->addSql('ALTER TABLE revscore DROP FOREIGN KEY FK_7B190456A76ED395');
        $this->addSql('DROP TABLE hat');
        $this->addSql('DROP TABLE revcard');
        $this->addSql('DROP TABLE reverso');
        $this->addSql('DROP TABLE revscore');
    }
}
