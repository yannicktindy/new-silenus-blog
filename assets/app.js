/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// import './styles/app.css';

// start the Stimulus application
import './bootstrap';
// import './js/dayNight.js';
import * as bootstrap from 'bootstrap';
console.log('pageModal ok');
// import * as bootstrap from 'bootstrap'

// ------------------- pop modal ----------------------
const MODAL_DELAY = 700;
async function load() {
    console.log('popModal ok');
    await new Promise(resolve => setTimeout(resolve, MODAL_DELAY));
    openModal();
}

function openModal() {
    console.log('popModal ok');
    const modal = document.getElementById('popModal');
    modal.style.opacity = 1;
    modal.style.pointerEvents = 'auto';
    const content = document.querySelector('.modl__content');
    content.style.transform = 'translateY(0)';
}
// ------------------- pop modal ----------------------

window.onload = setTimeout(function() {
    const modalTest = new bootstrap.Modal('#pageModal');
    modalTest.show();
}, 500);

import { Carousel } from 'bootstrap';

document.addEventListener('DOMContentLoaded', function() {
  // Trouvez l'élément du carousel dans votre DOM
  const carouselEl = document.querySelector('.carousel');

  // Créez une nouvelle instance de Carousel à partir de l'élément trouvé
  const carousel = new Carousel(carouselEl);
});

