const cards = document.querySelectorAll('.card');
const restart = document.getElementById('restartButton');
// const reversoMsg = document.querySelector('.reversoMsg');
const score = document.getElementById('score');
const moves = document.getElementById('move');
const result = document.getElementById('result');
const input = document.querySelector('input');
restart.classList.add('d-none');
let flippedCards = [];
let matchedCards = [];
let scoreCount = 0;
let moveCount = 0;
let finalResult = 0;

cards.forEach(card => {
  card.addEventListener('click', function() {
    if (canFlipCard(card)) {
      flipCard(card);
      if (flippedCards.length === 2) {
        checkForMatch();
      }
    }
  });
});

function canFlipCard(card) {
  return !card.classList.contains('flipped') && flippedCards.length < 2 && !matchedCards.includes(card);
}

function flipCard(card) {
  card.classList.add('flipped');
  flippedCards.push(card);
}

function randomInRange(min, max) {
  return Math.random() * (max - min) + min;
}


function checkForMatch() {
  const card1 = flippedCards[0];
  const card2 = flippedCards[1];
  const name1 = card1.getAttribute('name');
  const name2 = card2.getAttribute('name');

  if (name1 === name2) {
    matchedCards.push(card1, card2);
    scoreCount++;
    moveCount++;
    score.innerHTML = `${scoreCount} Paires`;
    moves.innerHTML = `${moveCount} Mouvements`;
    if (checkGameEnd()) {
        // add innerHTML to of victory message to reversoMsg
        score.innerHTML = 'Victoire ';
        moves.innerHTML = `${moveCount} Mouvements`;
        const finalResult = calculPoints();
        result.innerHTML = `${finalResult} Points !`;
        input.value = moveCount.toString();
        restart.classList.remove('d-none');
        // reversoMsg.innerHTML = 'Félicitations ! Vous avez terminé le jeu.';
        function shoot() {
          confetti(
          ); 
        }
        setTimeout(shoot, 0);
        setTimeout(shoot, 300);
        setTimeout(shoot, 600);
    } 
    else {
        // reversoMsg.innerHTML = 'Vous avez trouvé une paire !';
        confetti();
        setTimeout(() => {
            // reversoMsg.innerHTML = '[ ]';
          }, 2000);
      }
  } else {
    moveCount++;
    moves.innerHTML = `${moveCount} Mouvements`;
    // reversoMsg.innerHTML = 'Ce n\'est pas une paire !';
    setTimeout(() => {
      card1.classList.remove('flipped');
      card2.classList.remove('flipped');
    }, 500);
  }

  flippedCards = [];
}

function checkGameEnd() {
  return matchedCards.length === cards.length;
}

function calculPoints() {

  switch (true) {
    case moveCount < 7:
      finalResult = 100;
      break;
    case moveCount < 10:
      finalResult = 50;
      break
    case moveCount < 15:
      finalResult = 25; 
      break   
    case moveCount < 20:
      finalResult = 10;
      break;
    default:
      finalResult = 5;
      break;
  }
  return finalResult;
}


document.addEventListener('DOMContentLoaded', function() {
  restartGame();
});

  
function restartGame() {
    const cardsContainer = document.querySelector('.tapi');
    const cards = Array.from(document.querySelectorAll('.card'));
    restart.classList.add('d-none');
    // Réinitialiser les cartes retournées et correspondantes
    flippedCards = [];
    matchedCards = [];
    // reversoMsg.innerHTML = '[ ]';
    score.innerHTML = '0 Paire';
    moves.innerHTML = '0 Mouvement';
    result.innerHTML = '?';
    scoreCount = 0;
    moveCount = 0;

    // Retirer la classe "flipped" de toutes les cartes
    cards.forEach(card => {
      card.classList.remove('flipped');
    });
    
    // Retirer les cartes du DOM
    cards.forEach(card => {
      cardsContainer.removeChild(card);
    });
    
    // Mélanger les cartes
    shuffle(cards);
    
    // Ajouter les cartes mélangées au DOM avec un délai entre chaque carte
    cards.forEach((card, index) => {
      setTimeout(() => {
        cardsContainer.appendChild(card);
      }, 250 * index);
    });
    
    // Réinitialiser le score
    score.innerHTML = '0 Paire';
    moves.innerHTML = '0 Mouvement';
  }
  
  function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }

