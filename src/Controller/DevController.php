<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DevController extends AbstractController
{
    #[Route('/dev', name: 'app_dev')]
    public function index(
        ArticleRepository $articleRepository,
    ): Response
    {
        $article = $articleRepository->find(2);
        dump($article);
        return $this->render('dev/index.html.twig', [
            'controller_name' => 'DevController',
            'article' => $article
        ]);
    }
}
