<?php

namespace App\Controller\Admin;

use App\Entity\Revcard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;


class RevcardCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Revcard::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('img'),
            AssociationField::new('reverso'),
        ];
    }
}
