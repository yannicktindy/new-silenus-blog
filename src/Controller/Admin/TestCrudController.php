<?php

namespace App\Controller\Admin;

use App\Entity\Test;
use Doctrine\Common\Collections\Collection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TestCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Test::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
            TextField::new('descr'),
            CollectionField::new('questions')
                ->setLabel('Questions')
                ->setRequired(true)
                ->allowAdd() 
                ->allowDelete(false),
                // ->useEntryCrudForm(),
            CollectionField::new('factors')
                ->setLabel('Facteurs')
                ->setRequired(true)
                ->allowAdd() 
                ->allowDelete(false)

        ];
    }
    
}
