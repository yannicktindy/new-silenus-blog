<?php

namespace App\Controller\Admin;

use App\Entity\Hat;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use phpDocumentor\Reflection\Types\Float_;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Validator\Constraints\Choice;

class HatCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Hat::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
            TextField::new('img'),
            ChoiceField::new('matter')->setChoices([
                'cuirs' => 'cuirs',
                'feutres' => 'feutres',
            ]),
            ChoiceField::new('shape')->setChoices([
                'Melon' => 'melons',
                'hauts-de-forme' => 'hauts-de-forme',
                'casquettes' => 'casquettes',
            ]),
            BooleanField::new('isPromo'),
            IntegerField::new('quantity'),
            IntegerField::new('price'),
        ];
    }
    
}
