<?php

namespace App\Controller\Admin;

use App\Entity\DevLogo;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Filesystem\Filesystem;

class DevLogoCrudController extends AbstractCrudController
{
    
    public const ACTION_DUPLICATE = 'duplicate';
    public const TEST_BASE_PATH = 'uploads/img/devlogo';
    public const TEST_UPLOAD_PATH = 'public/' . self::TEST_BASE_PATH;
    
    
    public static function getEntityFqcn(): string
    {
        return DevLogo::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name')
                ->setLabel('Nom de l\'image')
                ->setRequired(true),
            ImageField::new('link')
                ->setLabel('Sélectionner une image')
                ->setUploadDir($this::TEST_UPLOAD_PATH)
                ->setUploadedFileNamePattern($this::TEST_BASE_PATH.'/'.'[randomhash].[extension]')
                ->setRequired(true)
            ];
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        // Get the path of the file to be deleted
        $filePath = $entityInstance->getName();
    
        // Delete the file if it exists
        if ($filePath && file_exists($filePath)) {
            $filesystem = new Filesystem();
            $filesystem->remove($filePath);
        }
    
        // Call the parent method to delete the entity from the database
        $entityManager->remove($entityInstance);
        $entityManager->flush();
    }
}
