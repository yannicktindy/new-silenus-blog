<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\ArticleCateg;
use DateTime;
use Doctrine\Persistence\Event\LifecycleEventArgs as EventLifecycleEventArgs;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\String\Slugger\AsciiSlugger;

use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class ArticleCrudController extends AbstractCrudController
{
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }    
    
    public static function getEntityFqcn(): string
    {
        return Article::class;
    }

  
    public function configureFields(string $pageName): iterable
    {

        return [
            yield IdField::new('id')->onlyOnIndex(),
            yield TextField::new('title'),
            yield TextField::new('slug'),
            yield TextField::new('categSlug'),
            yield TextEditorField::new('description')->onlyOnForms(),
            yield TextareaField::new('content')
                ->onlyOnForms()
                ->renderAsHtml(),
            yield TextField::new('slug')->onlyOnIndex(),
            yield IntegerField::new('count')->onlyOnIndex(),
            // yield DateTimeField::new('createdAt')->onlyOnIndex(),
            yield DateTimeField::new('updatedAt')->onlyOnForms(),
            yield AssociationField::new('logo')->onlyOnForms()
                ->autocomplete(),
            yield AssociationField::new('categories')
                ->autocomplete(),
            yield AssociationField::new('tags')
                ->autocomplete(),
            yield BooleanField::new('isIntro'),
            yield BooleanField::new('isActive'),
            yield BooleanField::new('inProgress'),
        ];
    }

    public function prePersist(EventLifecycleEventArgs $event) 
    {
        $entity = $event->getObject();
        
        if(!$entity instanceof Article) {
            return;
        }

        if($entity->getId() === null) {
            // Nouvel article
            
            $entity->setCreatedAt(new DateTime());
            $entity->setCount(0);
        
        } else {
            // Article existant
            //concatenation des categories séparées par des -
            $categSlug = '';
            foreach($entity->getCategories() as $category) {
                $categSlug .= $category->getTitle() . '-';
            }
            $entity->setCategSlug($categSlug);
            $entity->setUpdatedAt(new DateTime()); 
            $slugger = new AsciiSlugger();
            $slug = $slugger->slug($entity->getTitle());
            $entity->setSlug($slug);
            // $entity->setSlug($slugger->slug($entity->getTitle()));
        }
    }
    
}
