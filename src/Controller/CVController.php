<?php

namespace App\Controller;

use App\Entity\Visit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CVController extends AbstractController
{


    #[Route('/maintenance', name: 'app_cv_maintenance')]
    public function symfony(): Response
    {
        $newVisit = new Visit();
        $newVisit->setUpdatedAt(new \DateTime('now'));
        
        return $this->render('cv/maintenance.html.twig', [
            'controller_name' => 'CVController',
        ]);
    }

    #[Route('/', name: 'app_symfony')]
    public function dev(): Response
    {
        $newVisit = new Visit();
        $newVisit->setUpdatedAt(new \DateTime('now'));
        
        return $this->render('cv/newSymfony.html.twig', [
            'controller_name' => 'CVController',
        ]);
    }

    #[Route('/cv', name: 'app_new_cv')]
    public function newCv(): Response
    {
        $newVisit = new Visit();
        $newVisit->setUpdatedAt(new \DateTime('now'));
        
        return $this->render('cv/newCV.html.twig', [
            'controller_name' => 'CVController',
        ]);
    }

    #[Route('/projets', name: 'app_projets')]
    public function projet(): Response
    {
        $newVisit = new Visit();
        $newVisit->setUpdatedAt(new \DateTime('now'));
        
        return $this->render('cv/projets.html.twig', [
            'controller_name' => 'CVController',
        ]);
    }
}
