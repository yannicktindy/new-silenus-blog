<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InfoController extends AbstractController
{
    #[Route('/info/mentions-legales', name: 'app_info_legals')]
    public function legals(): Response
    {
        return $this->render('info/legals.html.twig', [
            'controller_name' => 'InfoController',
        ]);
    }

    #[Route('/info/a-propos', name: 'app_info_about')]
    public function about(): Response
    {
        return $this->render('info/about.html.twig', [
            'controller_name' => 'InfoController',
        ]);
    }

    #[Route('/info/conditions-générales-d-utilisation', name: 'app_info_cgu')]
    public function cgu(): Response
    {
        return $this->render('info/cgu.html.twig', [
            'controller_name' => 'InfoController',
        ]);
    }
}
