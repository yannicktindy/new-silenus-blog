<?php

namespace App\Controller;

use App\Repository\HatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

use function PHPUnit\Framework\isEmpty;

class HatController extends AbstractController
{
    #[Route('/hat/enter', name: 'app_hat_enter', methods: ['GET', 'POST'])]
    public function enter(
        HatRepository $hatRepository,
        Request $request
    ): Response
    {

        return $this->render('hat/enter.html.twig', [
        ]);
    }

    #[Route('/hat', name: 'app_hat', methods: ['GET', 'POST'])]
    public function index(
        HatRepository $hatRepository,
        Request $request
    ): Response
    {
        $hats = $hatRepository->findAll();
        $message = '';

        $session = $request->getSession();
        // $curentPanier = [];
        $nbHat = 0;

        if ($session->has('panier')) {
            $panier = $session->get('panier');
            foreach ($panier as $line) {
                $nbHat += $line['qty'];
                // $currentPanier[] = [
                //     'hat' => $hatRepository->findOneBy(['id' => $line['id']]),
                //     'qty' => $line['qty']
                // ];
            } 
            $message = $nbHat . ' Chapeaux dans votre panier';   
        } else {
            $message = 'Votre panier est vide';
        }

        return $this->render('hat/index.html.twig', [
            'hats' => $hats,
            'message' => $message,
            // 'currentPanier' => $currentPanier,
            'nbHat' => $nbHat
        ]);
    }

    #[Route('/hat/show/{id}', name: 'app_hat_show')]
        public function show(
            string $id,
            HatRepository $hatRepository,
            Request $request
        ): Response
        {

        $hat = $hatRepository->findOneBy(['id' => $id]);
        if (!$hat) {
            return $this->redirectToRoute('app_hat');
        }
        $message = 'C\'est le chapeau qu\'il vous faut !';

        // verifier si il y a un panier dans la session
        $session = $request->getSession();
        $curentPanier = [];
        $nbHat = 0;

        if ($session->has('panier')) {
            $panier = $session->get('panier');
            foreach ($panier as $line) {
                $nbHat += $line['qty'];
            } 
        } 

        if ($request->isMethod('POST')) {
            $quantity = $request->request->get('quantity');
            
            $session = $request->getSession();
            $panier = $session->get('panier', []);
            
            $availableHat = $hat->getQuantity();
            $hatName = $hat->getName();
            $hatExists = false;
            
            foreach ($panier as $index => $item) {
                if ($item['id'] === $id) {
                    $hatExists = true;
                    // Check if adding the quantity exceeds the available quantity
                    if ($panier[$index]['qty'] + $quantity > $availableHat) {
                        $quantityToAdd = $availableHat - $panier[$index]['qty'];
                        if ($quantityToAdd > 0) {
                            $panier[$index]['qty'] += $quantityToAdd;
                            $message = '⚠️ La quantité demandée est supérieure à la quantité disponible. Seulement ' . $quantityToAdd . ' ' . $hatName . ' ont été ajoutés à votre panier.';
                        } else {
                            $message = '⚠️ Le chapeau ' . $hatName . ' est déjà dans votre panier avec la quantité maximale disponible.';
                        }
                    } else {
                        $panier[$index]['qty'] += $quantity;
                        $message = $quantity . ' ' . $hatName . ' ont été ajoutés à votre panier.';
                    }
                    break;
                }
            }
            
            if (!$hatExists) {
                if ($quantity > $availableHat) {
                    $message = '⚠️ La quantité demandée est supérieure à la quantité disponible. Seulement ' . $availableHat . ' ' . $hatName . ' ont été ajoutés à votre panier.';
                    $quantity = $availableHat;
                }
                $panier[] = [
                    'id' => $id,
                    'qty' => $quantity
                ];
                $message = $quantity . ' ' . $hatName . ' ont été ajoutés à votre panier.';
            }
            
            $session->set('panier', $panier);
            // Redirect to the appropriate route or display a success message
            return $this->render('hat/show.html.twig', [
                'hat' => $hat,
                'message' => $message,
            ]);
        }

        return $this->render('hat/show.html.twig', [
            'hat' => $hat,
            'message' => $message,
            'nbHat' => $nbHat
        ]);
    }

    #[Route('/hat/basket', name: 'app_hat_basket')]
    public function basket(
        HatRepository $hatRepository,
        Request $request
    ): Response
    {
        $session = $request->getSession();
        $currentPanier = [];
        $nbHat = 0;
        $sum = 0;
        $message = '';

        if ($session->has('panier')) {
            $panier = $session->get('panier');
            foreach ($panier as $line) {
                $nbHat += $line['qty'];
                $currentPanier[] = [
                    'hat' => $hatRepository->findOneBy(['id' => $line['id']]),
                    'qty' => $line['qty']
                ];
                $sum += $line['qty'] * $hatRepository->findOneBy(['id' => $line['id']])->getPrice();
            } 
               
        } else {
            $message = 'Votre panier est vide';
        }


        return $this->render('hat/basket.html.twig', [
            'message' => $message,
            'panier' => $currentPanier,
            'sum' => $sum,
            'nbHat' => $nbHat
        ]);
    }

    #[Route('/hat/remove/{id}', name: 'app_hat_remove')]
    public function remove($id, SessionInterface $session)
    {
        $panier = $session->get('panier', []);
        foreach ($panier as $index => $item) {
            if ($item['id'] === $id) {
                unset($panier[$index]);
                break;
            }
        }
        $session->set('panier', $panier);

        return $this->redirectToRoute("app_hat_basket");
    }

    #[Route('/hat/cookie', name: 'app_hat_cookie')]
    public function cookie(SessionInterface $session)
    {
        $cookieHat = 'cookieOk';
        $session->set('cookieHat', $cookieHat);
        return $this->redirectToRoute("app_hat");
    }

    #[Route('/hat/close', name: 'app_hat_close')]
    public function close(SessionInterface $session)
    {
        $session->clear();
        return $this->render('hat/end.html.twig', [

        ]);
            
    }

}
