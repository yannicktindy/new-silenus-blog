<?php

namespace App\Controller;

use App\Form\TestFormType;
use App\Repository\FactorRepository;
use App\Repository\TestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    #[Route('/test', name: 'app_test')]
    public function index(): Response
    {
        return $this->render('test/newStart.html.twig', [
        ]);
    }

    // #[Route('/test/run', name: 'app_test_run')]
    // public function run(
    //     RequestStack $requestStack,
    //     SessionInterface $session,
    //     TestRepository $testRepository,
    // ): Response
    // {
    //     // $cookieTest = $session->get('cookieTest');
    //     // if (!$cookieTest) {
    //     //     return $this->redirectToRoute("app_test");
    //     // }

    //     $test = $testRepository->findOneBy(['id' => 1]);
    //     $quests = $test->getQuestions();
    //     $step = count($quests);
    //     $countQuests = $step;  

    //     return $this->render('test/run.html.twig', [
    //         // 'testForm' => $testForm->createView(),
    //         'step' => $step,
    //         'test' => $test,
    //         'quests' => $quests,
    //         'countQuests' => $countQuests,

    //     ]);
    // }

    #[Route('/test/show', name: 'app_test_run')]
    public function show(
        Request $request,
        SessionInterface $session,
        TestRepository $testRepository,
    
    ): Response
    {

        $test = $testRepository->findOneBy(['id' => 1]);
        $factors = $test->getFactors();
        $quests = $test->getQuestions();
        $arrayQuests = [];
        foreach($quests as $quest) {
            $arrayQuests[] = [
                'name' => $quest->getQuestion(),
                'factorId' => $quest->getFactor()->getId(),
            ];
        }
        shuffle($arrayQuests);


        // $step = count($quests);
        $countQuests = count($quests); 
        $scores = [];

        if ($request->isMethod('POST')) {
            $choices = [];
            $factorIds = [];

            foreach ($factors as $factor) {
                $force = 0;
                
                // Parcourir les questions
                for ($i = 0; $i < $countQuests; $i++) {
                    $choices[$i] = $request->request->get('choices_' . $i);
                    $factorId = $request->request->get('factorId_' . $i);
                    
                    if ($factorId == $factor->getId()) {
                        $force += $choices[$i];
                    }
                }
                
                // Stocker le factorId et la force associée
                $factorIds[] = $factor->getId();
                $forces[] = $force;
            }
            
            // Réorganiser les tableaux pour avoir le résultat souhaité
            $scores = [];
            for ($i = 0; $i < count($factorIds); $i++) {
                $scores[] = [
                    'factorid' => $factorIds[$i],
                    'force' => $forces[$i],
                ];
            }

            $session->set('scores', $scores);
            return $this->redirectToRoute('app_test_result');
        }
          
           

        return $this->render('test/show.html.twig', [
            // 'testForm' => $testForm->createView(),
            // 'step' => $step,
            'test' => $test,
            'countQuests' => $countQuests,
            'arrayQuests' => $arrayQuests,
        ]);
    }

    #[Route('/test/result', name: 'app_test_result')]
    public function result(
        SessionInterface $session,
        TestRepository $testRepository,
        FactorRepository $factorRepository,
        )
    {

        $test = $testRepository->findOneBy(['id' => 1]);
        $sessionScores = $session->get('scores');
        $scores = [];
        foreach ($sessionScores as $sessionScore) {
            $factor = $factorRepository->findOneBy(['id' => $sessionScore['factorid']]);
            $force =  $sessionScore['force'] / 3 * 2;
            $forceFormatted = number_format($force, 0);
            $scores[] = [
                'factor' => $factor,
                'force' => $forceFormatted,
            ];
        }
        // sort $scores by force
        usort($scores, function($a, $b) {
            return $b['force'] <=> $a['force'];
        });
        
        return $this->render('test/result.html.twig', [
            'test' => $test,
            'scores' => $scores,

        ]);
    }

    #[Route('/test/cookie', name: 'app_test_cookie')]
    public function cookie(SessionInterface $session)
    {
        $cookieTest = 'cookieTest';
        $session->set('cookieTest', $cookieTest);
        return $this->redirectToRoute("app_test_run");
    }

    #[Route('/test/close', name: 'app_test_close')]
    public function close(SessionInterface $session)
    {
        $session->clear();
        return $this->redirectToRoute("app_test");
    }
}
