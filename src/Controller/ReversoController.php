<?php

namespace App\Controller;

use App\Repository\RevcardRepository;
use App\Repository\ReversoRepository;
use App\Repository\RevscoreRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class ReversoController extends AbstractController
{
    #[Route('/reverso', name: 'app_reverso')]
    public function index(
        EntityManagerInterface $entityManager,
        ReversoRepository $reversoRepository,
        RevcardRepository $revcardRepository,
        UserRepository $userRepository,
        RevscoreRepository $revscoreRepository,
        SessionInterface $session,
        Request $request
    ): Response
    {
        // $cookieReverso = $session->get('cookieReverso');
        // if (!$cookieReverso) {
        //     return $this->redirectToRoute("app_reverso_enter");
        // }

        $revscore = $session->get('revscore');
        if (!$revscore) {
            // set count to 1 and points to $points ins session
            $revscore = [
                'count' => 0,
                'points' => 0,
            ];
        } 

        $reverso = $reversoRepository->find(1);
        $revcards = $revcardRepository->findBy(['reverso' => $reverso]);
        $revcards = array_merge($revcards, $revcards);
        shuffle($revcards);


        if ($request->isMethod('POST')) {
            $points = 0;
            // Récupérer les valeurs du formulaire
            $moves = intval($request->request->get('moves'));
            switch (true) {
                case ($moves < 7):
                    $points = 100;
                    break;
                case ($moves < 10):
                    $points = 50;
                    break;
                case ($moves < 15):
                    $points = 25;
                    break;
                case ($moves < 20):
                    $points = 10;
                    break;
                default:
                    $points = 5;
                    break;
            }
            // store result in session
            
            if (!$revscore) {
                // set count to 1 and points to $points ins session
                $revscore = [
                    'count' => 1,
                    'points' => $points,
                ];
            } else {
                // increment count and add points to $points ins session
                $revscore['count']++;
                $revscore['points'] += $points;
            }
            $session->set('revscore', $revscore);
        }    

        return $this->render('reverso/index.html.twig', [
            'reverso' => $reverso,
            'revcards' => $revcards,
            'revscore' => $revscore,
        ]);
    }

    #[Route('/reverso/enter', name: 'app_reverso_enter')]
    public function enter(
        ReversoRepository $reversoRepository
        
    ): Response
    {
        
        $reverso = $reversoRepository->find(1);

        return $this->render('reverso/enter.html.twig', [
            'reverso' => $reverso,
        ]);
    }

    #[Route('/reverso/cookie', name: 'app_reverso_cookie')]
    public function cookie(SessionInterface $session)
    {
        $cookieReverso = 'cookieReverso';
        $session->set('cookieReverso', $cookieReverso);
        return $this->redirectToRoute("app_reverso");
    }

    #[Route('/reverso/close', name: 'app_reverso_close')]
    public function close(SessionInterface $session)
    {
        $session->clear();
        return $this->redirectToRoute("app_reverso_enter");
    }
}


