<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class TestFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $iteration = $options['data']['countQuests'];
        // iteration = number of questions
        for ($i = 0; $i < $iteration; $i++) {
            $builder           
                ->add('choices'.$i, ChoiceType::class, [
                    'choices' => [
                        '1' => 0,
                        '2' => 8,
                        '3' => 16,
                        '4' => 24,
                        '5' => 32,
                        '6' => 40,
                        '7' => 50,
                    ],
                    'expanded' => true,
                    'multiple' => false,
                    'required' => true,
                    'label' => false,
                    'choice_name' => null, 
                    'choice_attr' => function($choice, $key, $value) {
                        // Ajoutez la classe 'form-check-input' à chaque élément de choix
                        return ['class' => 'form-check-input'];
                    },
                    'attr' => [
                        'class' => 'form-check-container' // Classe pour le conteneur des radios
                    ],
                    
                ]);
                // ->add('factor'.$i, TextType::class, [
                //     'required' => true,
                //     'label' => 'factor ID',
                //     'attr' => [
                //         'class' => 'form-control', 'd-none'// Ajoutez ici les classes Bootstrap que vous souhaitez utiliser
                //     ],  
                // ]);
        }
    }
}