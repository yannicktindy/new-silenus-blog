<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\RevcardRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RevcardRepository::class)]
// #[ApiResource]
class Revcard
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $img = null;

    #[ORM\ManyToOne(inversedBy: 'cards')]
    private ?Reverso $reverso = null;

    #[ORM\Column(length: 10)]
    private ?string $name = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getReverso(): ?Reverso
    {
        return $this->reverso;
    }

    public function setReverso(?Reverso $reverso): self
    {
        $this->reverso = $reverso;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString(){
        return $this->name;
    }
}