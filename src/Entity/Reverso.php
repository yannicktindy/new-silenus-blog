<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ReversoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[ORM\Entity(repositoryClass: ReversoRepository::class)]
// #[ApiResource]

class Reverso
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'reverso', targetEntity: Revcard::class)]
    private Collection $cards;

    #[ORM\Column(length: 40)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'reverso', targetEntity: Revscore::class)]
    private Collection $Scores;

    #[ORM\Column(length: 255)]
    private ?string $img = null;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
        $this->Scores = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }



    /**
     * @return Collection<int, Revcard>
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Revcard $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards->add($card);
            $card->setReverso($this);
        }

        return $this;
    }

    public function removeCard(Revcard $card): self
    {
        if ($this->cards->removeElement($card)) {
            // set the owning side to null (unless already changed)
            if ($card->getReverso() === $this) {
                $card->setReverso(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Revscore>
     */
    public function getScores(): Collection
    {
        return $this->Scores;
    }

    public function addScore(Revscore $score): self
    {
        if (!$this->Scores->contains($score)) {
            $this->Scores->add($score);
            $score->setReverso($this);
        }

        return $this;
    }

    public function removeScore(Revscore $score): self
    {
        if ($this->Scores->removeElement($score)) {
            // set the owning side to null (unless already changed)
            if ($score->getReverso() === $this) {
                $score->setReverso(null);
            }
        }

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }
}
