<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $content = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
    private ?bool $isIntro = null;

    #[ORM\Column]
    private ?bool $isActive = null;

    #[ORM\Column]
    private ?bool $inProgress = null;

    #[ORM\Column]
    private ?int $count = null;

    #[ORM\Column(length: 255)]
    private ?string $categSlug = null;

    #[ORM\ManyToMany(targetEntity: ArticleTag::class, inversedBy: 'articles')]
    private Collection $tags;

    #[ORM\ManyToMany(targetEntity: ArticleCateg::class, inversedBy: 'articles')]
    private Collection $categories;

    #[ORM\ManyToMany(targetEntity: DevLogo::class, inversedBy: 'articles')]
    private Collection $logos;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: ArticleCom::class)]
    private Collection $articleComs;

    public function __construct()
    {
        $this->count = 0;
        $this->createdAt = new DateTime();
        $this->tags = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->logos = new ArrayCollection();
        $this->articleComs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsIntro(): ?bool
    {
        return $this->isIntro;
    }

    public function setIsIntro(bool $isIntro): self
    {
        $this->isIntro = $isIntro;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function isInProgress(): ?bool
    {
        return $this->inProgress;
    }

    public function setInProgress(bool $inProgress): self
    {
        $this->inProgress = $inProgress;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getCategSlug(): ?string
    {
        return $this->categSlug;
    }

    public function setCategSlug(string $categSlug): self
    {
        $this->categSlug = $categSlug;

        return $this;
    }

    /**
     * @return Collection<int, ArticleTag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(ArticleTag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(ArticleTag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * @return Collection<int, ArticleCateg>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(ArticleCateg $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }

        return $this;
    }

    public function removeCategory(ArticleCateg $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * @return Collection<int, DevLogo>
     */
    public function getLogos(): Collection
    {
        return $this->logos;
    }

    public function addLogo(DevLogo $logo): self
    {
        if (!$this->logos->contains($logo)) {
            $this->logos->add($logo);
        }

        return $this;
    }

    public function removeLogo(DevLogo $logo): self
    {
        $this->logos->removeElement($logo);

        return $this;
    }

    /**
     * @return Collection<int, ArticleCom>
     */
    public function getArticleComs(): Collection
    {
        return $this->articleComs;
    }

    public function addArticleCom(ArticleCom $articleCom): self
    {
        if (!$this->articleComs->contains($articleCom)) {
            $this->articleComs->add($articleCom);
            $articleCom->setArticle($this);
        }

        return $this;
    }

    public function removeArticleCom(ArticleCom $articleCom): self
    {
        if ($this->articleComs->removeElement($articleCom)) {
            // set the owning side to null (unless already changed)
            if ($articleCom->getArticle() === $this) {
                $articleCom->setArticle(null);
            }
        }

        return $this;
    }


}
