<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TestRepository::class)]
class Test
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $descr = null;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Factor::class)]
    private Collection $Factors;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: Question::class)]
    private Collection $questions;

    public function __construct()
    {
        $this->Factors = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescr(): ?string
    {
        return $this->descr;
    }

    public function setDescr(string $descr): self
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * @return Collection<int, Factor>
     */
    public function getFactors(): Collection
    {
        return $this->Factors;
    }

    public function addFactor(Factor $factor): self
    {
        if (!$this->Factors->contains($factor)) {
            $this->Factors->add($factor);
            $factor->setTest($this);
        }

        return $this;
    }

    public function removeFactor(Factor $factor): self
    {
        if ($this->Factors->removeElement($factor)) {
            // set the owning side to null (unless already changed)
            if ($factor->getTest() === $this) {
                $factor->setTest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setTest($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getTest() === $this) {
                $question->setTest(null);
            }
        }

        return $this;
    }
}
